#include "PVKernelServer.h"

// STL includes
#include <memory>
#include <thread>

// zmq includes
#include <zmq_addon.hpp>

// xeus includes
#include <xeus/xkernel_configuration.hpp>
#include <xeus/xserver_zmq.hpp>

// Qt includes
#include <QTimer>

//-----------------------------------------------------------------------------
PVKernelServer::PVKernelServer(zmq::context_t& context, const xeus::xconfiguration& config)
  : xserver_zmq(context, config)
{
  // 50ms interval is sort enough so that users will not notice significant latency
  // yet it is long enough to minimize CPU load caused by polling.
  m_pollTimer = new QTimer();
  m_pollTimer->setInterval(50);
  QObject::connect(m_pollTimer, &QTimer::timeout, [=]() { poll(0); });
}

//-----------------------------------------------------------------------------
PVKernelServer::~PVKernelServer()
{
  m_pollTimer->stop();
  delete m_pollTimer;
}

//-----------------------------------------------------------------------------
void PVKernelServer::start_impl(zmq::multipart_t& message)
{
  start_publisher_thread();
  start_heartbeat_thread();

  m_request_stop = false;
  m_pollTimer->start();

  publish(message, xeus::channel::SHELL);
}

//-----------------------------------------------------------------------------
void PVKernelServer::stop_impl()
{
  this->xserver_zmq::stop_impl();
  m_pollTimer->stop();
  stop_channels();
  std::this_thread::sleep_for(std::chrono::milliseconds(50));
}

//-----------------------------------------------------------------------------
std::unique_ptr<xeus::xserver> make_PVKernelServer(
  zmq::context_t& context, const xeus::xconfiguration& config)
{
  return std::make_unique<PVKernelServer>(context, config);
}
