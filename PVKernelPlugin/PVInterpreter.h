#pragma once

// xeus
#include "xeus/xinterpreter.hpp"
using xeus::xinterpreter;

// paraview / vtk
class pqPythonShellCompleter;

#include "vtkSmartPointer.h"
class vtkObject;
class vtkPythonInteractiveInterpreter;

// stl
#include <string>

class PVInterpreter : public xinterpreter
{

public:
  PVInterpreter();
  virtual ~PVInterpreter() = default;

private:
  void configure_impl() override;

  /**
   * Executes the given code on the kernel.
   *
   * Forward the string to the paraview python interpreter.
   * Also handle `Display()` command to request a screenshot.
   */
  nl::json execute_request_impl(int execution_counter, const std::string& code, bool silent,
    bool store_history, nl::json user_expressions, bool allow_stdin) override;

  /**
   * Returns a list of suggestion for code completion.
   */
  nl::json complete_request_impl(const std::string& code, int cursor_pos) override;

  /**
   * Returns informations about the context.
   *
   * Typically, it is used by notebook to create tooltip.
   */
  nl::json inspect_request_impl(const std::string& code, int cursor_pos, int detail_level) override;

  /**
   * Decides whether to immediately execute the current code,
   * or whether to show a continuation prompt for further input.
   * Mainly usefull in command line interface.
   *
   * For instance, in Python a = 5 would be executed immediately, while for i in range(5): would
   * expect further input.
   *
   * For now, returns always 'complete', i.e. code should be executed.
   */
  nl::json is_complete_request_impl(const std::string& code) override;

  /**
   * Returns general purpose informations about kernel.
   */
  nl::json kernel_info_request_impl() override;

  void shutdown_request_impl() override;

  void handle_events(vtkObject* caller, unsigned long eventId, void* callData);

  bool m_initialized;
  std::vector<std::string> m_error_messages;
  std::vector<std::string> m_output_messages;
  vtkSmartPointer<vtkPythonInteractiveInterpreter> m_interpreter;
  pqPythonShellCompleter* m_completer;
};
