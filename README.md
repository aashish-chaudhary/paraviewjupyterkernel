# ParaViewJupyter

## Bring paraview python shell to your browser

This is a Jupyter kernel for ParaView, based on [xeus](https://quantstack.net/xeus).

It allows to remotely control a full ParaView application through the jupyter notebook technology.

*by N.Vuaille (nicolas.vuaille@kitware.com), Kitware SAS 2019*

Funded by Total SAS

## Features

Start with Examples/Demo.ipynb for an overview !

Use it as the classic python shell of ParaView, whith following specific features:

 * 'paraview.simple' is imported by default
 * Screenshots can be displayed in the notebook with different options:

``` python
   # default 400x300 image from the current view
   def Display(w = 400, h = 300, all_views = False)
```

Works on linux. Should work on OSX. May work on Windows in the future
(limitation due to the use of the `env` program to launch the kernel)

## Setup

This repo contains the configuration file for jupyter (`kernel.json`) and the source code for the ParaView kernel
that can be built as a ParaView plugin.

### Prerequisites

* Built version of ParaView version 5.7+, with Python activated (python 2 and 3 are supported)
* Xeus version 0.23.2+

### Build

1. install xeus 0.23.2.

  With conda, follow this instructions https://github.com/QuantStack/xeus#installation.

  Or build it from sources https://github.com/QuantStack/xeus#building-from-source. Be sure to respect the dependecies versions ! You may have to build some of them.

2. build ParaView

  Full instructions are here : https://gitlab.kitware.com/paraview/paraview/blob/master/Documentation/dev/build.md

3. build ParaView Jupyter Plugin

 * go to a build directory
 * configure with cmake :
   * set `ParaView_DIR` to your build directory of ParaView
   * if you built xeus (and not install), set `xeus_DIR` to its build dir
 * build: `cmake --build .`

Variable of interest:
 * `PARAVIEW_EXECUTABLE`: the ParaView program that will be launched. Default deduced from `ParaView_DIR`.
 * `KERNEL_INSTALL_PATH`: a standard dir for kernel configurations files. See jupyter documentation. Default is a linux user configuration.
 * `PLUGIN_INSTALL_PATH`: the directory where to find the built plugin. Default is a subdirectory of `CMAKE_INSTALL_PREFIX`

### Run

* Launch a notebook

```bash
$ jupyter notebook
```

* Go to your browser
* Select the 'ParaView' entry in the 'new' menu or open an existing Notebook (as Demo.ipynb provided in this repo).  A ParaView client will pop up on the server side
* Start writing paraview-python code in the notebook ! ParaView client is updated as usual in ParaView python mode and can be used to inspect the results.

